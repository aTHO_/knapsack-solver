#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#*******************************************************************************************#
#*----- Auteur :        Aubertin Emmanuel               |  For: Watcher-Soft            ****#
#*----- GitHub :        https://github.com/Athomisos    | Twitter : @BlenderAubertin    ****#
#*----- Description :   Knapsack solver                                                 ****#
#*******************************************************************************************#

'''
DESCRIPTION :
  * Knapsack solver in python.
'''

# DATASET : https://people.sc.fsu.edu/~jburkardt/datasets/knapsack_01/knapsack_01.html

__app__ = 'Knapsack-solver'
__author__ = "Aubertin Emmanuel"
__copyright__ = "2021, CERI"
__credits__ = "Aubertin Emmanuel"
__license__ = "GPL"
__version__ = "1.0.0"

import argparse, sys, logging
from Knapsack import Knapsack

def print_split(SplitName):
    print("\n----|\t\x1b[1m",SplitName,"\x1b[0m\t|----\t")

def print_done():
    print("\t\x1b[32mDone\x1b[0m")

def PrintArray(inArr):
    for item in inArr:
        print("|\t", end="")
        for obj in item:
            print(str(obj) + "\t|\t", end="")
        print("\n")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="""
        Knapsack solver
        """,
        usage="""
            main.py -n 4 -b 7 
        """,
        epilog="version {}, license {}, copyright {}, credits {}".format(__version__,__license__,__copyright__,__credits__))
    parser.add_argument('-n', '--number', type=int, nargs='?', help='Number of object', default=4)
    parser.add_argument('-b', '--backpack', type=int, nargs='?', help='Capacity of the backpack', default=7)
    parser.add_argument('-w', '--weigth', type=str, nargs='?', help='Weihght of object.')
    parser.add_argument('-p', '--pvalue', type=str, nargs='?', help='Value of object.')
    parser.add_argument('-v', '--verbose', help='be verbose', action='store_true')

    args = parser.parse_args()

    logger = logging.getLogger(__app__)
    VERBOSE = args.verbose
    weigth = args.weigth
    pvalue = args.pvalue

    print("Welcome in the Knapsack solver :)")
    print("version {} | {}, {}".format(__version__,__credits__,__copyright__))

    n = int(args.number)
    b = int(args.backpack)

    print_split("Creation of objectList")
    object_weight = [] # [(aj, cj)] aj-> pound | cj -> value
    object_value = []
    f = open(weigth, "r")
    for x in f:
        object_weight.append(int(x))
        if(VERBOSE):
            print(x)


    f = open(pvalue, "r")
    i = 0
    for x in f:
        object_value.append(int(x))
        i += 1
        if(VERBOSE):
            print(x)

       
    if(n != len(object_value) and n != len(object_weight)):
        sys.exit("ERROR : Object missing (or excessing) in objectList")
    print_done()

    print_split("Initialisation of Knapsack Object")
    backpack = Knapsack(n, b, object_weight, object_value);
    if(VERBOSE):
        backpack.to_print()
    print_done()

    print_split("Dynamic Programation Solver")

    solution = backpack.knapsack_res(VERBOSE)
    print_done()
    
    print_split("SOLUTION")
    print(solution)

    print("______________________________")  
    print_split("Good bye :)")
    print("______________________________")
