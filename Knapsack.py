#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#*******************************************************************************************#
#*----- Auteur :        Aubertin Emmanuel               |  For: Watcher-Soft            ****#
#*----- GitHub :        https://github.com/Athomisos    | Twitter : @BlenderAubertin    ****#
#*----- Description :   Knapsack class                                                 ****#
#*******************************************************************************************#

'''
DESCRIPTION :
  * Knapsack class solver in python.
'''

__author__ = "Aubertin Emmanuel"
__copyright__ = "2022, CERI"
__credits__ = ["Aubertin Emmanuel"]
__license__ = "GPL"
__version__ = "1.1.0"

import logging
from tabnanny import verbose

class Knapsack:
  def __init__(self, inputN, inputB, object_weight, object_value):
    self.n  = inputN 
    self.b  = inputB  # +1 to from 0 to b capacity
    self.object_weight = object_weight
    self.object_value = object_value

  def info():
    print("Welcome in the Knapsack solver :)")
    print("version {} | {}, {}".format(__version__,__credits__,__copyright__))

  def to_print(self):
    print("Knapsack Object : \n\
    \tCapacity : " + str(self.b) + ";\n\
    \tNumber of Objet : " + str(self.n) + ";\n\
    \tList of object :\n\
    \t\tValue :  ", end="")
    for item in self.object_value:
      print(str(item) + "  |  ", end="")
    print("\n\t\tWeight : ", end="")
    for item in self.object_weight:
      print(str(item) + "  |  ", end="")
    print()

  def get_array(self, x, y): # Plus tard 
    ouput = []
    for _ in range(0, x):
      ouput.append( [0] * y)
    return ouput

  # CREDIT : https://towardsdatascience.com/dynamic-program-vs-integer-program-which-one-is-better-for-the-knapsack-problem-759f41b9755d
  def knapsack_res(self, verbose):  
    weight_threshold = self.b
    weight_list = self.object_weight
    util_list = self.object_value
    n = self.n
    lookup_table = [[0 for x in range(weight_threshold+1)] 
                    for x in range(n+1)] 
  
    for i in range(n+1):
      if(verbose):
        print("TESTING object number : " + str(i))
      for w in range(weight_threshold+1):
        if i==0 or w==0:
          lookup_table[i][w] = 0
        elif weight_list[i-1] <= w:
          lookup_table[i][w] = max(
            util_list[i-1] 
                + lookup_table[i-1][w-weight_list[i-1]], 
                lookup_table[i-1][w])
        else: 
          lookup_table[i][w] = lookup_table[i-1][w]
      if(verbose):
        print("\tRESULT :", end=' ')
        print(lookup_table[i])
    if(verbose):
      for i in lookup_table:
        print(i)
    return lookup_table[n][weight_threshold]